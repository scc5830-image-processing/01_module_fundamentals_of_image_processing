#!/usr/bin/env python
# coding: utf-8

# Angelo Victor Kraemer Foletto
## Num USP: 12620258
### SCC0251/5830— Prof. Moacir A. Ponti
### Teaching Assistant: Leo Sampaio Ferraz Ribeiro
### Assignment 1 : image generation
# ---

### Import block
import numpy as np
import imageio
import random
import math


### Dynamics Variables
imgPath       = str(input().rstrip())
lateralSizeC  = int(input().rstrip()) # value: C
functionF     = int(input().rstrip()) # value: (1, 2, 3, 4 or 5)
imgGeneration = int(input().rstrip()) # value: Q
lateralSizeN  = int(input().rstrip()) # where N <= C
numPixel      = int(input().rstrip()) # value: B; 1 <= B <= 8
seed          = int(input().rstrip()) # value: S

# ---

### Generating a Synthetic Image

#### 1- f(x, y) = (xy + 2y)
def image0(x, y):
    return float((x * y) + (2 * y))


#### 2- f(x, y) = |cos(x/Q) + 2 sin(y/Q)|

def image1(x, y):
    return float(np.abs((np.cos(x / imgGeneration)) + (2 * np.sin(y / imgGeneration))))


#### 3- f(x, y) = |3(x/Q) − (y/Q)^3|
def image2(x, y):
    return float(np.abs((3 * (x / imgGeneration)) - ((y / imgGeneration) ** (1/3))))


#### 4- f(x, y) = rand(0, 1, S)
def image3():
    db = np.zeros([lateralSizeC,lateralSizeC], dtype="float")
    random.seed(seed)
    for y in range(lateralSizeC):
        for x in range(lateralSizeC):
            db[x, y] = float(random.uniform(0, 1))
    return db


#### 5- f(x, y) = randomwalk(S)
# 
# x = [(x + dx) % C] 
# y = [(y + dy) % C]^2 
def image4():
    db = np.zeros([lateralSizeC, lateralSizeC], dtype="float")
    count = int(1 + (lateralSizeC * lateralSizeC))
    random.seed(seed)
    x = 0
    y = 0
    db[x, y] = float(1)

    while count >= 0:
        x = int((x + random.randint(-1, 1)) % lateralSizeC)
        y = int(math.pow((y + random.randint(-1, 1)) % lateralSizeC , 1))
        db[x, y] = float(1)
        count -= 1
    return db


#### Utils 
# Create image function
def createImg(): 
    arraImage = np.zeros([lateralSizeC, lateralSizeC], dtype=float)
    inpt = imgPath.split('.')[0]
    for x in range(arraImage.shape[0]):
        for y in range(arraImage.shape[1]):
            if inpt == 'ex1':
                arraImage[x][y] = image0(x, y)
            elif inpt == 'ex2':
                arraImage[x][y] = image1(x, y)
            elif inpt == 'ex3':
                arraImage[x][y] = image2(x, y)
            else:
                continue

    if inpt == 'ex4':
        return image3()
    elif inpt == 'ex5':
        return image4()
    else:
        return arraImage

# ---

### Sampling and Quantizing the Image
#### Downsampling
def regions(db):
    if lateralSizeN > lateralSizeC:
        return False
    swap = int(lateralSizeC / lateralSizeN) # C/N
    return db[::swap,::swap]


#### Quantizing
def quantizing(db):
    min = np.min(db)
    max = np.max(db)
    
    quantizing = float(255 / (max - min))
    for x in range(lateralSizeN):
        for y in range(lateralSizeN):
            db[x, y] = float((db[x, y] - min) * quantizing)
    return db.astype(np.uint8)


#### Bitwise
def bitwise(db):
    global numPixel
    numPixel = 8 - numPixel
    
    for x in range(db.shape[0]):
        for y in range(db.shape[1]):
            db[x, y] = db[x, y] >> numPixel
            db[x, y] = db[x, y] << numPixel
    return db

# ---

### Comparing against reference
def rse(db):
    expImg = np.load(imgPath).astype(np.uint8)
    swap = 0
    for x in range(db.shape[0]):
        for y in range(db.shape[1]):
            swap += (db[x, y] - expImg[x ,y]) ** 2
    return np.sqrt(swap)

#### Main
swap = createImg()
newImg = regions(swap)
newImg = quantizing(newImg)
newImg = bitwise(newImg)

print('%.4f' % rse(newImg))

